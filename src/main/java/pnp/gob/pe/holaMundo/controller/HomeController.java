package pnp.gob.pe.holaMundo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/")
	public String inicio() {
		return "Hola Mundo";
	}
	
	@GetMapping("/vamos")
	public String vamos() {
		return "Siempre vamos por mas";
	}
}
