package pnp.gob.pe.holaMundo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Demo {

	@GetMapping("/demo")
	public String inicio() {
		return "Hola mundo con el demo";
	}
	
	@GetMapping("/demoSaludo")
	public String inicioSaludo() {
		return "Hola mundo con el demo saludando a todos";
	}
}
